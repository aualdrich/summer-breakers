class WelcomeController < ApplicationController

 def index
    @summer_feed = []
    @global_summer_feed = []

    if current_user
      feed = instagram.user_media_feed({:count => 1000})

      feed.each do |f|
        if (f.caption != nil) && (f.caption.text.include? "summer")
          @summer_feed << f

        elsif (f.tags.present?) && (f.tags.include? "summer")
          @summer_feed << f
        end
      end

      tag_feed = instagram.tag_recent_media("summer")

      tag_feed.each do |f|
        @global_summer_feed << f
      end

    end

  end


  def show

  end

  protected 

  def instagram
    if !@instagram
      Instagram.configure do |config|
        config.client_id = ENV['INSTAGRAM_APP_ID']
        config.client_secret = ENV['INSTAGRAM_SECRET_KEY']
      end

      @instagram = Instagram.client(:access_token => current_user.token)
    else
      @instagram
    end
  end

end
